<div>
    <section class="max-w-4xl p-6 mx-auto my-28 bg-white rounded-md shadow-md dark:bg-gray-800">
        <h2 class="text-lg font-semibold text-gray-700 capitalize dark:text-white">Калькулятор КБЖУ</h2>

        <form enctype="multipart/form-data">
            <div class="grid grid-cols-1 gap-6 mt-4">
                <!--  Проверка для форм -->
                @if($loseWeight || $gainWeight)
                    <div>
                        @if($loseWeight)
                            <label class="text-gray-700 dark:text-yellow-300">Дефицит калорий 5% = {{ $min }}</label>
                            <label class="text-gray-700 dark:text-yellow-500"> <br> Дефицит калорий 30% = {{ $max }}</label>
                        @else
                            <label class="text-gray-700 dark:text-yellow-300">Профицит калорий 5% = {{ $min }}</label>
                            <label class="text-gray-700 dark:text-yellow-500"> <br> Профицит калорий 30% = {{ $max }}</label>
                        @endif
                    </div>
                @elseif($formStep == 1)
                <div>
                    <label class="text-gray-700 dark:text-gray-200" for="gender">Пол</label>
                    <select wire:model.lazy="gender" name="gender" id="gender" class="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring">
                        <option selected="selected" hidden>Выберите пол</option>
                        <option value="1">Мужской</option>
                        <option value="2">Женский</option>
                    </select>
                    @error('gender') <span class="text-red-400">{{ $message }}</span> @enderror
                </div>
                <div>
                    <label class="text-gray-700 dark:text-gray-200" for="weight">Вес</label>
                    <input wire:model.lazy="weight" name="weight" id="weight" type="number" class="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring">
                    @error('weight') <span class="text-red-400">{{ $message }}</span> @enderror
                </div>
                <div>
                    <label class="text-gray-700 dark:text-gray-200" for="height">Рост</label>
                    <input wire:model.lazy="height" name="height" id="height" type="number" class="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring">
                    @error('height') <span class="text-red-400">{{ $message }}</span> @enderror
                </div>
                <div>
                    <label class="text-gray-700 dark:text-gray-200" for="age">Возраст</label>
                    <input wire:model.lazy="age" name="age" id="age" type="number" class="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring">
                    @error('age') <span class="text-red-400">{{ $message }}</span> @enderror
                </div>
                @elseif($formStep == 2)
                <div>
                    <label class="text-gray-700 dark:text-green-500">Ваш уровень базового обмена веществ = {{$boo}}</label>
                    <label class="mt-4 text-gray-700 dark:text-gray-200"> <br> Коэффициент физической активности</label>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault1" value="1" checked>
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault1">
                            Офисная работа/сидячая
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault2" value="2">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault2">
                            Работа сидячая + 1−3 р/нед лёгкий спорт
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault3" value="3">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault3">
                            Работа на ногах (8−12 час) без подъёма тяжестей
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault4" value="4">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault4">
                            Работа сидячая + 3 р/нед интенсивный спорт
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault5" value="5">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault5">
                            Работа на ногах (от 8 час) + 3 р/нед интенсивный спорт
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault6" value="6">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault6">
                            Работа сидячая + интенсивный спорт 5 р/нед
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault7" value="7">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault7">
                            Работа на ногах (8−12 час) + интенсивный спорт 5 р/нед = «сдохнуть» можно
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault8" value="8">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault8">
                            Домохозяйки
                        </label>
                    </div>
                    <div class="mt-4 form-check">
                        <input wire:model.lazy="kfa" class="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault9" value="9">
                        <label class="form-check-label inline-block text-white" for="flexRadioDefault9">
                            Домохозяйки + огород
                        </label>
                    </div>
                </div>
                @elseif($formStep == 3)
                    <label class="text-gray-700 dark:text-green-400">Ваша суточная норма = {{ $norm }}</label>
                    <label class="text-gray-700 dark:text-gray-200">Вы хотите похудеть или набрать массу?</label>
                @endif
            </div>

            <!--  Проверка для кнопок -->
            @if($formStep == 1)
                <div class="flex justify-end mt-6">
                    <button type="button" wire:click="nextStep" class="px-6 py-2 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Далее</button>
                </div>
            @elseif($formStep == 2)
                <div class="flex justify-between mt-6">
                    <button type="button" wire:click="prevStep" class="px-6 py-2 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Назад</button>
                    <button type="button" wire:click="nextStep" class="px-6 py-2 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Далее</button>
                </div>
            @elseif($formStep == 3)
                <div class="flex justify-between mt-6">
                    <button type="button" wire:click="prevStep" class="px-6 py-2 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Назад</button>
                    <div>
                        <button type="button" wire:click="loseWeight" class="px-6 py-2 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Похудеть</button>
                        <button type="button" wire:click="gainWeight" class="px-6 py-2 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Набрать массу</button>
                    </div>
                </div>
            @else
                <div class="flex justify-between mt-6">
                    <button type="button" class="px-6 py-2 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">
                        <a href="{{ route('kjud.calc') }}">На главную</a></button>
                </div>
            @endif
        </form>
    </section>
</div>
