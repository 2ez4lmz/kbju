<?php

namespace App\Http\Livewire;

use Livewire\Component;

class KjudCalculate extends Component
{
    //Формы и шаги
    public $formStep = 1;
    public $loseWeight = false;
    public $gainWeight = false;

    //Переменные
    public $gender;
    public $weight;
    public $height;
    public $age;

    public $boo;

    public $kfa = 1;

    public $norm;
    public $min;
    public $max;

    //Правила для валидации
    protected $rules = [
        'gender' => 'required|in:"1","2"',
        'weight' => 'required|numeric|min:0|max:1000',
        'height' => 'required|numeric|min:20|max:280',
        'age' => 'required|numeric|min:0|max:120',
    ];

    //Реал тайм валидация
    public function updated($rules)
    {
        $this->validateOnly($rules);
    }

    //След. шаг
    public function nextStep()
    {
        //Шаг 1. Расчет BOO
        if($this->formStep == 1)
        {
            if($this->gender == 1)
            {
                $this->boo = ceil(66.5 + 13.75 * $this->weight + 5.003 * $this->height - 6.775 * $this->age);
            }
            else
            {
                $this->boo = ceil(655.1  + 9.563 * $this->weight + 1.85 * $this->height - 4.676 * $this->age);
            }
        }
        //Шаг 2. Расчет нормы
        if($this->formStep == 2)
        {
            switch($this->kfa)
            {
                case 1:
                    $this->kfa = 1.2;
                    break;
                case 2: case 8:
                    $this->kfa = 1.3;
                    break;
                case 3: case 9:
                    $this->kfa = 1.5;
                    break;
                case 4:
                    $this->kfa = 1.4;
                    break;
                case 5: case 6:
                    $this->kfa = 1.6;
                    break;
                case 7:
                    $this->kfa = 1.9;
                    break;
            }

            $this->norm = ceil($this->boo * $this->kfa);
        }
        $validatedData = $this->validate();
        $this->formStep++;
    }

    //Пред. шаг
    public function prevStep()
    {
        $this->formStep--;
    }

    //Форма похудения
    public function loseWeight()
    {
        $this->formStep++;
        $this->loseWeight = true;

        $this->min = ceil($this->norm  - $this->norm / 100 * 5);
        $this->max = ceil($this->norm  - $this->norm / 100 * 30);
    }

    //Форма набора
    public function gainWeight()
    {
        $this->formStep++;
        $this->gainWeight = true;

        $this->min = ceil($this->norm  + $this->norm / 100 * 5);
        $this->max = ceil($this->norm  + $this->norm / 100 * 30);
    }

    public function render()
    {
        return view('livewire.kjud-calculate');
    }
}
