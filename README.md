# Калькулятор КБЖУ

Ссылка: https://kbju.herokuapp.com/ 

## Запуск

* Run ``` git clone https://gitlab.com/2ez4lmz/kbju.git ```
* Go to the folder application using ``` cd kbju ```
* Run ``` composer install ```
* Run ``` copy .env.example .env ```
* Run ```php artisan key:generate ```
* Run ``` php artisan serve ```
* Go to http://localhost:8000/

## Скриншоты

![formStep1](https://iili.io/h7HIlS.jpg "Форма 1")
![formStep2](https://iili.io/h7HTU7.jpg "Форма 2")
![formStep3](https://iili.io/h7Hzf2.jpg "Форма 3")
![formStep4](https://iili.io/h7HAJ9.jpg "Форма 4")


